-- SUMMARY --

Codius application to run a Peer-to-peer Drupal installation.

For a full description of the module, visit the project page:
  http://drupal.org/project/codius
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/codius

-- REQUIREMENTS --

* Codius CLI
  Visit www.npmjs.com/package/codius for instructions to install the Codius command-line interface (CLI).

-- USAGE --

Download this module and then execute:

$ cd codius/manifest
$ codius upload ./drupal8-codius.json --host https://codius.codiusx.com

-- CREDITS --

Authors:
* Eleo Basili (eleonel) - http://drupal.org/u/eleonel

This project has been sponsored by Codius X (http://www.codiusx.com).

